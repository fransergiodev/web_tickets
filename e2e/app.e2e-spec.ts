import { TicketsWebPage } from './app.po';

describe('tickets-web App', () => {
  let page: TicketsWebPage;

  beforeEach(() => {
    page = new TicketsWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
